﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;

public class GridSystem : MonoBehaviour
{
    [Serializable]
    public struct Cell
    {
        public int x;
        public int y;

        public static bool operator ==(Cell c1, Cell c2)
        {
            return c1.x == c2.x && c1.y == c2.y;
        }

        public static bool operator !=(Cell c1, Cell c2)
        {
            return c1.x != c2.x || c1.y != c2.y;
        }
    }

    [Tooltip("Put the sandcastle here !")] public GameObject gridUnitReference;

    public Vector2 gridOffset;

    [Header("Debug")] public Vector2Int gridPreviewSize;
    public float gridHeight = 0.1f;

    private Vector2 _cellSize;
    private Dictionary<Cell, GameObject> _snappedObjects = new Dictionary<Cell, GameObject>();
    [AssetSelector,AssetList]
    [SerializeField] private List<Map> Maps;
    [SerializeField] private GameObject IndestructiblePrefab;
    [SerializeField] private GameObject DestructiblePrefab;

    void Start()
    {
        Vector3 bounds = gridUnitReference.GetComponent<MeshFilter>().sharedMesh.bounds.size;
        _cellSize = new Vector3(bounds.x, bounds.z);

        var map = Maps[Random.Range(0, Maps.Count)];
        // var indestructibleCastle = IndestructibleCastle[range];
        foreach (var cell in map.IndestructibleCastle)
        {
            var pos = GetPosFromCell(cell);
            AddObject(Instantiate(IndestructiblePrefab, pos+Vector3.up/2, Quaternion.identity, transform));
        }

        foreach (var cell in map.DestructibleCastle)
        {
            var pos = GetPosFromCell(cell);
            AddObject(Instantiate(DestructiblePrefab, pos+Vector3.up/2, Quaternion.identity, transform));
        }
    }

    public Cell GetCellFromPos(Vector3 position)
    {
        Vector2 withOffset = new Vector2(position.x - gridOffset.x, position.z - gridOffset.y);
        int x = (int) Mathf.Floor(withOffset.x / _cellSize.x);
        int y = (int) Mathf.Floor(withOffset.y / _cellSize.y);
        return new Cell {x = x, y = y};
    }

    public Vector3 GetPosFromCell(Cell cell)
    {
        return new Vector3(cell.x * _cellSize.x + gridOffset.x + _cellSize.x / 2, transform.position.y,
            cell.y * _cellSize.y + gridOffset.y + _cellSize.y / 2);
    }

    public bool CellFreeAt(Vector3 position)
    {
        Cell c = GetCellFromPos(position);
        return !_snappedObjects.ContainsKey(c);
    }

    public GameObject GetObjectAt(Vector3 position)
    {
        Cell c = GetCellFromPos(position);
        return _snappedObjects[c];
    }

    public Vector3 GetSnapedPosition(Vector3 position)
    {
        Cell c = GetCellFromPos(position);
        Vector3 newPos = GetPosFromCell(c);
        newPos.y = position.y; // We keep the y position
        return newPos;
    }

    public void AddObject(GameObject obj)
    {
        Cell c = GetCellFromPos(obj.transform.position);

        if (_snappedObjects.ContainsKey(c))
            return;

        _snappedObjects.Add(c, obj);
        Health health = obj.GetComponent<Health>();
        if (health == null) return;

        health.onDeath += (object o, System.EventArgs args) => { RemoveAtCell(c); };
    }

    private void RemoveAtCell(Cell cell)
    {
        _snappedObjects.Remove(cell);
    }

    private void OnDrawGizmosSelected()
    {
        if (!gridUnitReference) return;

        Vector3 bounds = gridUnitReference.GetComponent<MeshFilter>().sharedMesh.bounds.size;
        Vector2 cellSize = new Vector3(bounds.x, bounds.z);

        Gizmos.color = Color.red;
        Vector3 center = transform.position + new Vector3(gridOffset.x, 0.0f, gridOffset.y);

        float xOffset = (gridPreviewSize.x / 2) * cellSize.x;
        float yOffset = (gridPreviewSize.y / 2) * cellSize.y;

        for (int y = 0; y <= gridPreviewSize.y; y++)
        {
            Vector3 start = center - new Vector3(-xOffset, gridHeight, y * cellSize.y - yOffset);
            Vector3 end = center - new Vector3(xOffset, gridHeight, y * cellSize.y - yOffset);
            Gizmos.DrawLine(start, end);
        }

        for (int x = 0; x <= gridPreviewSize.x; x++)
        {
            Vector3 start = center - new Vector3(x * cellSize.x - xOffset, gridHeight, -yOffset);
            Vector3 end = center - new Vector3(x * cellSize.x - xOffset, gridHeight, yOffset);
            Gizmos.DrawLine(start, end);
        }
    }
}