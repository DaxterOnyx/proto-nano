﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBar : MonoBehaviour
{
    public Health Character;

    private Slider _slider; 
    
    // Start is called before the first frame update
    void Start()
    {
        _slider = GetComponent<Slider>();
        _slider.maxValue = Character.MaxHealthPoint;
    }

    // Update is called once per frame
    void Update()
    {
        
        _slider.value = Character.HealthPoint;
    }
}
