﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
public class Move : InputReader
{
    public float Speed = 2f;

    [Header("Jump mechanic")] public bool AllowDoubleJump = false;
    public float JumpHeight = 1f;
    [ShowIf("AllowDoubleJump")] public float SecondJumpHeight = 0.5f;


    [Header("Dash mechanic")] public bool DashInsteadOfJump = false;
    [ShowIf("DashInsteadOfJump")] public float DashSpeed = 5f;
    [ShowIf("DashInsteadOfJump")] public float DashMaxDistance = 1f;
    [ShowIf("DashInsteadOfJump")] public float DashCooldown = 1f;
    [ShowIf("DashInsteadOfJump")] public VisualEffect DashEffect;
    [ShowIf("DashInsteadOfJump")] public TrailRenderer DashTrail;

    private Rigidbody _rigidbody;
    private Vector2 _joystick;

    private bool _canDoubleJump;
    private bool _isAiming;

    private float DashMaxTime;
    private float DashTime = 0f;
    private Vector3 DashDirection;

    void Start()
    {
        DashMaxTime = DashMaxDistance / DashSpeed;
        _rigidbody = GetComponent<Rigidbody>();

        //Actions
        // actionAsset = new PlayerControl();
        // actionAsset.Enable();
        // actionAsset.Player.Move.performed += OnMove;
        // actionAsset.Player.Move.canceled += OnMove;
        // actionAsset.Player.Move.started += OnMove;
        // actionAsset.Player.Dash.performed += OnDash;
        // actionAsset.Player.Jump.performed += OnJump;


        Hand hand = GetComponent<Hand>();
        if (hand)
        {
            hand.onAim += () => _isAiming = true;
            hand.onUseItem += () => _isAiming = false;
            hand.onItemChange += () => _isAiming = false;
        }

        GetComponent<PlayerInput>().onActionTriggered += Input;
    }

    private void Input(InputAction.CallbackContext context)
    {
        switch (context.action.name)
        {
            case "Move" when context.performed:
                OnMove(context);
                break;
            case "Move" when context.canceled:
                StopMove(context);
                break;
            case "Dash" when context.performed:
                OnDash(context);
                break;
            case "Jump" when context.performed:
                OnJump(context);
                break;
        }
    }

    private void StopMove(InputAction.CallbackContext ctx)
    {
        if (IsGoodDevice(ctx))
        {
            Debug.Log("Stop Move");
            _joystick = Vector2.zero;
        }
    }

    private void Update()
    {
        var dashVelocity = Vector3.zero;
        DashTime += Time.deltaTime;
        if (DashTime < DashMaxTime)
        {
            dashVelocity = DashDirection * DashSpeed;
        }
        else
        {
            DashEffect.SendEvent("OnStop");
            DashTrail.emitting = false;
        }

        var direction = new Vector3(_joystick.x, 0.0f, _joystick.y);
        float trueSpeed = _isAiming ? 0 : Speed;
        Vector3 movement = direction * trueSpeed + dashVelocity;
        _rigidbody.velocity = new Vector3(movement.x, _rigidbody.velocity.y, movement.z);
        transform.LookAt(transform.position + direction);
    }

    private void OnMove(InputAction.CallbackContext context)
    {
        if (IsGoodDevice(context))
        {
            var readValue = context.ReadValue<Vector2>();
            _joystick = readValue;
        }
    }

    private void OnJump(InputAction.CallbackContext context)
    {
        if (!IsGoodDevice(context)) return;

        if (DashInsteadOfJump) return;
        //_rigidbody.AddForce(Vector3.up * GetJumpForce() ,ForceMode.VelocityChange); // ???

        bool secondJump = false;

        if (!IsGrounded())
        {
            if (!AllowDoubleJump || !_canDoubleJump) return;
            _canDoubleJump = false;
            secondJump = true;
        }
        else
        {
            _canDoubleJump = true;
        }

        _rigidbody.velocity = new Vector3(0.0f, GetJumpForce(secondJump), 0.0f);
    }

    private void OnDash(InputAction.CallbackContext context)
    {
        if (!IsGoodDevice(context)) return;
        if (!DashInsteadOfJump || DashTime < DashCooldown)
            return;

        DashTime = 0.0f;
        DashDirection = transform.forward;

        DashEffect.SendEvent("OnPlay");
        DashTrail.emitting = true;
    }

    private float GetJumpForce(bool secondJump)
    {
        float height = secondJump ? SecondJumpHeight : JumpHeight;
        return Mathf.Sqrt(-2.0f * Physics.gravity.y * height);
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, 1.1f);
    }
}