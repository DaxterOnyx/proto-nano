﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class Hand : InputReader
{
    public event Action onAim;
    public event Action onUseItem;
    public event Action onItemChange;

    public Transform HandPosition;

    private List<HandItem> _accessibleItem;
    private HandItem _currentHeld;

    void Awake()
    {
        // Pick.performed += context => OnPick(context);
        // actionAsset = new PlayerControl();
        // actionAsset.Enable();

        GetComponent<PlayerInput>().onActionTriggered += Input;
        // playerInput.onActionTriggered += ctx => OnPick(ctx);
        //  
        //  .Player.Pick.performed += context => OnPick(context);
        //    actionAsset.Player.UseItem.performed += context => OnAim(context);
        //    actionAsset.Player.UseItem.canceled += context => OnUseItem(context);
        _accessibleItem = new List<HandItem>();
    }

    private void Input(CallbackContext context)
    {
        //Debug.Log(context.action.name);
        switch (context.action.name)
        {
            case "Pick" when context.performed:
                OnPick(context);
                break;
            case "UseItem" when context.started:
                OnAim(context);
                break;
            case "UseItem" when context.canceled:
                OnUseItem(context);
                break;
        }
    }

    private void OnPick(CallbackContext context)
    {
        if (!IsGoodDevice(context)) return;
        if (!context.performed) return;

        var newItem = PickLastAccessibleItem();
        if (!newItem || newItem.Owner) return;

        Debug.Log("Pick item");
        if (_currentHeld)
        {
            _currentHeld.Drop(newItem.transform);
            _currentHeld.onBreak -= OnHandItemBreak;
        }

        _currentHeld = newItem;
        _currentHeld.onBreak += OnHandItemBreak;
        _currentHeld.Pick(this);
        onItemChange?.Invoke();
    }

    private void OnAim(CallbackContext context)
    {
        if (!_currentHeld) return;
        onAim?.Invoke();
    }

    private void OnUseItem(CallbackContext context)
    {
        if (!IsGoodDevice(context)) return;
        if (!_currentHeld) return;

        _currentHeld.Use();
        onUseItem?.Invoke();
    }

    private void OnHandItemBreak(object sender, EventArgs e)
    {
        _currentHeld = null;
        onItemChange?.Invoke();
    }

    private HandItem PickLastAccessibleItem()
    {
        if (_accessibleItem.Count == 0) return null;

        HandItem item = _accessibleItem[_accessibleItem.Count - 1];
        _accessibleItem.Remove(item);

        return item;
    }

    private void OnTriggerEnter(Collider other)
    {
        HandItem item = other.GetComponent<HandItem>();

        if (!item) return;
        if (_accessibleItem.Contains(item)) return;

        _accessibleItem.Add(item);
    }

    private void OnTriggerExit(Collider other)
    {
        HandItem item = other.GetComponent<HandItem>();

        if (!item) return;
        _accessibleItem.Remove(item);
    }
}