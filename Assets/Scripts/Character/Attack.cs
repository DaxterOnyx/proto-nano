﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.AccessControl;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.VFX;
using Random = UnityEngine.Random;

public class Attack : InputReader
{
    [AssetSelector, InlineEditor(InlineEditorObjectFieldModes.Foldout), OnValueChanged("ResetDebugCombo")]
    public List<Combo> Combos = new List<Combo>();

    #region Debug

    [Header("Debug"), OnValueChanged("ResetDebugCombo"), ShowInInspector]
    private bool IsDebug = false;

    private int DebugCombosCount
    {
        get
        {
            int c = Combos.Count - 1;
            if (c < 0)
                c = 0;
            return c;
        }
    }

    [Indent, ShowIf("IsDebug"), PropertyRange(0, "DebugCombosCount"), OnValueChanged("ResetDebugHit"), ShowInInspector]
    private int DebugComboIndex = 0;

    private int DebugHitsCount
    {
        get
        {
            if (Combos.IsNullOrEmpty() || Combos[DebugComboIndex].Hits.IsNullOrEmpty()) return 0;
            int c = Combos[DebugComboIndex].Hits.Count - 1;
            if (c < 0)
                c = 0;
            return c;
        }
    }

    [Indent, ShowIf("IsDebug"), PropertyRange(0, "DebugHitsCount"), ShowInInspector]
    private int DebugHitIndex = 0;

    private void OnDrawGizmos()
    {
        if (IsDebug)
        {
            Gizmos.color = Color.red;
            var hit = GetHit(DebugComboIndex, DebugHitIndex);
            if (hit != null)
                Gizmos.DrawWireSphere(AttackRange(DebugComboIndex, DebugHitIndex), hit.AreaOfAttack);
        }
    }

    void ResetDebugCombo()
    {
        DebugComboIndex = 0;
        ResetDebugHit();
    }

    void ResetDebugHit()
    {
        DebugHitIndex = 0;
    }

    #endregion

    [Header("Settings")] public VisualEffectAsset AttackVisualEffect;
    public VisualEffectAsset ChargingVisualEffect;

    private Vector3 AttackRange(int comboIndex, int hitIndex)
    {
        var hit = GetHit(comboIndex, hitIndex);
        if (hit is null)
        {
            return transform.forward;
        }

        return transform.position + (transform.rotation * hit.Range);
    }

    private Hit GetHit(int comboIndex, int hitIndex)
    {
        if (comboIndex >= Combos.Count)
        {
            Debug.LogError("Try to get Combo not defined : index = " + comboIndex);
            return null;
        }

        if (hitIndex >= Combos[comboIndex].Hits.Count)
        {
            Debug.LogError("Try to get Hit not defined in combo : combo = " + Combos[comboIndex].name +
                           ", hitIndex = " + hitIndex);
            return null;
        }

        return Combos[comboIndex].Hits[hitIndex];
    }


    private enum State
    {
        Nothing,
        Preparing,
        Reposing
    }

    [ReadOnly, ShowInInspector] private int _hitIndex;
    [ReadOnly, ShowInInspector] private List<int> _combosIndex;
    [ReadOnly, ShowInInspector] private float _cooldown;
    [ReadOnly, ShowInInspector] private State _state = State.Nothing;
    [ReadOnly, ShowInInspector] private bool _comboValidate = false;
    [ReadOnly, ShowInInspector] private float _timePressed = 0;

    private Hit.Signal _pressed = global::Hit.Signal.None;
    private GameObject _chargingVFX = null;

    private void Start()
    {
        // if (actionAsset is null)
        // {
        //     actionAsset = new PlayerControl();
        //     actionAsset.Enable();
        // }

        // actionAsset.Player.Melee.started += OnMeleePressed;
        // actionAsset.Player.Melee.canceled += OnMeleeRelease;
        // actionAsset.Player.Dist.started += OnDistPressed;
        // actionAsset.Player.Dist.canceled += OnDistRelease;

        GetComponent<PlayerInput>().onActionTriggered += Input;
    }

    private void Input(InputAction.CallbackContext context)
    {
        //Debug.Log(context.action.name);
        switch (context.action.name)
        {
            case "Melee" when context.started:
                OnMeleePressed(context);
                break;
            case "Melee" when context.canceled:
                OnMeleeRelease(context);
                break;
            case "Dist" when context.started:
                OnDistPressed(context);
                break;
            case "Dist" when context.canceled:
                OnDistRelease(context);
                break;
        }
    }

    private void OnMeleePressed(InputAction.CallbackContext context)
    {
        // if (context.control.device.deviceId != deviceId) return;
        StartPress(global::Hit.Signal.Melee);
    }

    private void OnMeleeRelease(InputAction.CallbackContext context)
    {
        // if (context.control.device.deviceId != deviceId) return;
        StopPress(global::Hit.Signal.Melee);
    }

    private void OnDistPressed(InputAction.CallbackContext context)
    {
        // if (context.control.device.deviceId != deviceId) return;
        StartPress(global::Hit.Signal.Distance);
    }

    private void OnDistRelease(InputAction.CallbackContext context)
    {
        // if (context.control.device.deviceId != deviceId) return;
        StopPress(global::Hit.Signal.Distance);
    }

    private void StartPress(Hit.Signal signal)
    {
        _pressed = signal;
        if (!(_state == State.Preparing && GetHit(_combosIndex[0], _hitIndex).ChargingAttack))
            _timePressed = 0;
    }

    private void StopPress(Hit.Signal signal)
    {
        if (_pressed == signal)
        {
            switch (_state)
            {
                case State.Nothing:
                case State.Reposing:
                    //Start a new Combo
                    //Search hit correponding signal and Time 
                    _combosIndex = PossibleHit(signal, _timePressed, 0);

                    if (!_combosIndex.IsNullOrEmpty())
                        StartState(State.Preparing);
                    break;
                case State.Preparing:
                    var list = PossibleHit(signal, 0, _hitIndex + 1);
                    if (!list.IsNullOrEmpty())
                        _comboValidate = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        else
        {
            Debug.LogWarning("Release another key, it is weird");
        }

        _pressed = global::Hit.Signal.None;
    }

    private List<int> PossibleHit(Hit.Signal signal, float timePressed, int hitIndex)
    {
        var comboList = new List<int>();

        for (int index = 0; index < Combos.Count; index++)
        {
            var combo = Combos[index];
            if (combo.Hits.Count > hitIndex
                && combo.Hits[hitIndex].Action == signal
                && combo.Hits[hitIndex].MinTimePress <= timePressed
                && combo.Hits[hitIndex].MaxTimePress >= timePressed)
            {
                comboList.Add(index);
            }
        }

        return comboList;
    }

    private void StartState(State state)
    {
        _state = state;
        switch (state)
        {
            case State.Preparing:
                _comboValidate = false;

                if (_chargingVFX != null)
                {
                    Destroy(_chargingVFX);
                    _chargingVFX = null;
                }

                _cooldown += GetHit(_combosIndex[0], _hitIndex).AnticipationTime;
                break;
            case State.Nothing:
                if (_chargingVFX != null)
                {
                    Destroy(_chargingVFX);
                    _chargingVFX = null;
                }

                break;
            case State.Reposing:
                _cooldown += GetHit(_combosIndex[0], _hitIndex).RecoveryTime;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void UpdateState()
    {
        if (_pressed != global::Hit.Signal.None)
        {
            _timePressed += Time.deltaTime;
        }

        switch (_state)
        {
            case State.Nothing:
                var possibleHit = PossibleHit(_pressed, _timePressed, _hitIndex);
                if (_pressed != global::Hit.Signal.None
                    && possibleHit.Count == 1)
                {
                    //Charging
                    if (_chargingVFX.SafeIsUnityNull())
                    {
                        //VFX
                        _chargingVFX = new GameObject("Charging VFX");
                        _chargingVFX.transform.position = AttackRange(possibleHit[0], 0);
                        _chargingVFX.transform.parent = transform;
                        var vfxve = _chargingVFX.AddComponent<VisualEffect>();
                        vfxve.visualEffectAsset = ChargingVisualEffect;
                        var hit = GetHit(possibleHit[0], 0);
                        vfxve.SetFloat("Lifetime", hit.MaxTimePress - hit.MinTimePress);
                    }


                    //END CHARGING
                    if (PossibleHit(_pressed, _timePressed + Time.deltaTime, _hitIndex).IsNullOrEmpty())
                    {
                        StopPress(_pressed);
                    }
                }

                break;
            case State.Preparing:
            case State.Reposing:
                _cooldown -= Time.deltaTime;
                if (_cooldown <= 0)
                {
                    ExitState();
                }

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void ExitState()
    {
        switch (_state)
        {
            case State.Nothing:
                break;
            case State.Preparing:
                Hit();
                break;
            case State.Reposing:
                StartState(State.Nothing);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Hit()
    {
        if (_combosIndex.IsNullOrEmpty()) _combosIndex = PossibleHit(_pressed, _timePressed, _hitIndex);
        var hit = GetHit(_combosIndex[0], _hitIndex);
        string debug = "Attacking with " + hit.name + " : ";

        var targets = Physics.OverlapSphere(AttackRange(_combosIndex[0], _hitIndex), hit.AreaOfAttack);
        foreach (var target in targets)
        {
            if (target.gameObject == gameObject)
                continue;

            Health targetHealth = target.GetComponent<Health>();
            if (!(targetHealth is null))
            {
                debug += target.name + " ";
                MakeDamage(targetHealth, hit);
            }
        }

        if (IsDebug) Debug.Log(debug);

        if (_comboValidate)
        {
            _hitIndex++;
            StartState(State.Preparing);
        }
        else
        {
            _hitIndex = 0;
            StartState(State.Reposing);
        }
    }

    private void MakeDamage(Health target, Hit hit)
    {
        int hitDamage = hit.Damage;
        if (hit.ChargingAttack)
        {
            var timeCharged = (_timePressed) / hit.MaxTimePress;
            var f = hit.DamageProgression.Evaluate(timeCharged);
            Debug.Log(timeCharged + "->" + f);
            hitDamage = Mathf.RoundToInt(hit.MinDamage +
                                         ((hit.Damage - hit.MinDamage) *
                                          f));
        }

        target.TakeDamage(hitDamage);

        //VFX
        var vfx = new GameObject("Attack VFX");
        vfx.transform.position = target.transform.position;
        var vfxve = vfx.AddComponent<VisualEffect>();
        vfxve.visualEffectAsset = AttackVisualEffect;
        vfxve.SetUInt("NbParticule", hit.NbParticule);
        vfxve.SetFloat("Duration", hit.Duration);
        vfxve.SetFloat("MinLifeDuration", hit.LifeDuration.x);
        vfxve.SetFloat("MaxLifeDuration", hit.LifeDuration.y);
        vfxve.SetFloat("Velocity", hit.Velocity);
        if (!(hit.ColorOverLife is null))
            vfxve.SetGradient("ColorOverLife", hit.ColorOverLife);
        if (!(hit.ParticuleTexture is null))
            vfxve.SetTexture("Texture", hit.ParticuleTexture);
        vfxve.SendEvent("OnPlay");
        vfxve.Play();

        Destroy(vfx, hit.Duration + hit.LifeDuration.y + 0.1f);
    }

    private void Update()
    {
        UpdateState();
    }
}
// projection ombre et outline