﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Indicators : MonoBehaviour
{
    public RectTransform BuildPreview;
    public Image BuildPreviewImage;


    void Start()
    {
        BuildPreview.gameObject.SetActive(false);
    }

    public void EnableBuildPreview(bool enable)
	{
        BuildPreview.gameObject.SetActive(enable);
	}

    public void MoveBuildPreview(Vector3 position, Quaternion rotation, bool valid)
	{
        BuildPreview.position = position;
        BuildPreview.rotation = rotation;
        BuildPreviewImage.color = valid ? Color.green : Color.red;
	}
}
