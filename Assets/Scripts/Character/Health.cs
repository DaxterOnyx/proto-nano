﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

[RequireComponent(typeof(Collider))]
public class Health : MonoBehaviour
{
    public Life MyLife;
    public event EventHandler onDeath;

    [SerializeField] internal int MaxHealthPoint => (int) MyLife.Health;
    public int HealthPoint { get; private set; }
    [SerializeField] private bool IsPlayer = false;
    [ShowIf("IsPlayer")] public int PlayerIndex;
    [SerializeField] private GameObject PrefabPopup;
    [SerializeField] private Color PopupColor = Color.white;
    // Start is called before the first frame update
    
    void Start()
    {
        HealthPoint = MaxHealthPoint;
    }

    public void TakeDamage(int damage)
    {
        HealthPoint -= damage;
        if (HealthPoint <= 0)
            Die();

        //Popup
        var popup = Instantiate(PrefabPopup).GetComponent<DamagePopup>();
        popup.transform.position = transform.position;
        popup.Setup(damage,PopupColor);
    }

    void Die()
    {
        onDeath?.Invoke(this, EventArgs.Empty);
        if (IsPlayer)
            RoundsManager.instance.WinRoundFor(PlayerIndex);
        else
        {
            Destroy(gameObject);
        }
    }
}