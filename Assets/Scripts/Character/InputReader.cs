﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
public class InputReader : MonoBehaviour
{
    internal int deviceId;
    protected PlayerInput playerInput;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
    }

    protected bool IsGoodDevice(InputAction.CallbackContext context)
    {
        return true;
        var deviceDeviceId = context.control.device.deviceId;
        if (deviceDeviceId != deviceId)
        {
            //Debug.LogWarning("Controller waited : " + deviceId + " Controller received : " + deviceDeviceId);
            return false;
        }

        return true;
    }
}