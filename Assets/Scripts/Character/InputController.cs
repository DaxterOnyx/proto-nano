﻿using System.Collections;
using System.Collections.Generic;
using ControllerVisualizer;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class InputController : MonoBehaviour
{
    
    public int controlIndex
    {
        get => _ControlIndex;
        set
        {
            _ControlIndex = value;
            ResolveControl();
        }
    }
    
    [SerializeField] private int _ControlIndex = 0;
    
    private void Awake() {
        ResolveControl();
        InputSystem.onDeviceChange += (device, change) => ResolveControl();
    }

    internal void ResolveControl()
    {
        InputReader[] readers = GetComponents<InputReader>();
        var gamepads = Gamepad.all;
        if (gamepads.Count <= this.controlIndex) {
            Debug.LogError("Gamepad n°"+controlIndex+" not found.");
            return;
        }
        var deviceId = gamepads[_ControlIndex].deviceId;
        foreach (InputReader reader in readers)
        {
            reader.deviceId = deviceId;
            // reader.actionAsset.Enable();
        }
    }
    
    
}
