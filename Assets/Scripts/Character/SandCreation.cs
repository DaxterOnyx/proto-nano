﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.VFX;

public class SandCreation : MonoBehaviour
{
    [Header("Values")]

    [MinValue(0.01f)]
    public float HeightSandCastle = 1;
    [MinValue(0.01f)]
    public float ScaleSandCastle = 1;
    [MinValue(0)]
    public float CooldownSpawn = 1;
    private float lastTimeSpawn = 0;
    
    public float DistToCreate = 0;
    
    [Header("Setup")]
    public GameObject SandCastlePrefab;
    public bool IsDebug = false;
    private Mesh sharedMesh;
    public VisualEffectAsset SandEffect;
    
    public Vector3 TrueScaleSandCastle => new Vector3(ScaleSandCastle, HeightSandCastle, ScaleSandCastle);
    public Vector3 TruePosSandCastle => transform.position + (transform.forward*(ScaleSandCastle/2+DistToCreate+transform.lossyScale.z/2)) + (Vector3.up*(-transform.position.y+HeightSandCastle/2));
    
    private void Start()
    {
        sharedMesh = SandCastlePrefab.GetComponent<MeshFilter>().sharedMesh;
    }

    private void OnDrawGizmos()
    {
        if (IsDebug)
        {
            Gizmos.color = Color.green;
            if(sharedMesh == null) 
                sharedMesh = SandCastlePrefab.GetComponent<MeshFilter>().sharedMesh;
            
            Gizmos.DrawWireMesh(sharedMesh,
                TruePosSandCastle,
                transform.rotation,TrueScaleSandCastle);
        }
    }

    public void Update()
    {
        if (lastTimeSpawn > 0)
            lastTimeSpawn -= Time.deltaTime;
    }


    public void OnCreate()
    {
        if (lastTimeSpawn <= 0)
        {
            var health = Instantiate(SandCastlePrefab, TruePosSandCastle, transform.rotation).GetComponent<Health>();
            health.transform.localScale = TrueScaleSandCastle; 
            lastTimeSpawn = CooldownSpawn;
            
            var vfx = new GameObject("Sand VFX");
            vfx.transform.position = TruePosSandCastle + Vector3.down*0.5f;
            var vfxve = vfx.AddComponent<VisualEffect>();
            vfxve.visualEffectAsset = SandEffect;
            //vfxve.;
            Destroy(vfx,5);
        }
    }
}