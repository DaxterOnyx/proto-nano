﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

namespace Character
{
    [RequireComponent(typeof(Animator))]
    public class AttackAnimated : InputReader
    {
        [Header("Settings")] [SerializeField] private VisualEffectAsset AttackVisualEffect;
        [SerializeField] private VisualEffect ChargingVFX = null;
        private Animator _animator;

        #region Combos Manager

        [SerializeField] private float MaxTimeCharging = 2.5f;
        [SerializeField] private float TimeBetweenCombo = 1f;

        [ReadOnly, ShowInInspector] private int _comboState = 0;
        [ReadOnly, ShowInInspector] private bool _isCharging = false;
        [ReadOnly, ShowInInspector] private bool _isPressing = false;

        [ReadOnly, ShowInInspector] private bool _isAttacking = false;
        [ReadOnly, ShowInInspector] private bool _isCombo = false;

        // [ReadOnly, ShowInInspector] private bool _isComboting= false;
        [ReadOnly, ShowInInspector] private float _timePressed = 0;
        [ReadOnly, ShowInInspector] private float _timeCharged = 0;
        [ReadOnly, ShowInInspector] private float _timeLastHit = 0;

        #endregion


        private void Start()
        {
            _animator = GetComponent<Animator>();

            GetComponent<PlayerInput>().onActionTriggered += Input;
        }


        private void Update()
        {

            //Charging attack
            if (_isCharging)
            {
                _timeCharged += Time.deltaTime;
                if (_timeCharged >= MaxTimeCharging)
                {
                    EndCharging();
                }
            }
        }

        #region InputGestion

        private void Input(InputAction.CallbackContext context)
        {
            switch (context.action.name)
            {
                case "Melee" when context.started:
                    // Debug.Log(context.action.name);
                    OnMeleePressed(context);
                    break;
                case "Melee" when context.canceled:
                    // Debug.Log(context.action.name);
                    OnMeleeRelease(context);
                    break;
            }
        }

        private void OnMeleePressed(InputAction.CallbackContext ctx)
        {
            _isPressing = true;
            _timePressed = 0;
            _animator.SetTrigger("Attack");
        }

        private void OnMeleeRelease(InputAction.CallbackContext ctx)
        {
            _isPressing = false;
            _timePressed = 0;
            if (_isCharging)
            {
                EndCharging();
            }

            if (!_isAttacking)
            {
                if (_comboState >= 2 || !(Time.time <= _timeLastHit + TimeBetweenCombo))
                {
                    _comboState = 0;
                }
            }
        }

        #endregion

        public void StartAnticipation()
        {
            _timePressed = 0;
            _isAttacking = true;
        }

        public void EndAnticipation()
        {
            if (_isPressing)
            {
                StartCharging();
            }
            else
            {
                StartAttack();
            }
        }

        private void StartAttack()
        {
            if(_comboState < 2)
                _animator.Play("Punch");
            else
                _animator.Play("Kick");
        }

        public void StartCharging()
        {
            _isCharging = true;
            _timeCharged = 0;
            ChargingVFX.gameObject.SetActive(true);
            ChargingVFX.SendEvent("OnPlay");

            _animator.Play("Charging");
        }

        public void EndCharging()
        {
            _isCharging = false;
            ChargingVFX.gameObject.SetActive(false);
            ChargingVFX.SendEvent("OnStop");

            _animator.Play("Charged");
            _isPressing = false;
        }

        private void Hit(Hit hit)
        {
            var myTransform = transform;
            var targets =
                Physics.OverlapSphere(myTransform.position + (myTransform.rotation * hit.Range), hit.AreaOfAttack);
            foreach (var target in targets)
            {
                if (target.gameObject == gameObject)
                    continue;

                Health targetHealth = target.GetComponent<Health>();
                if (!(targetHealth is null))
                {
                    MakeDamage(targetHealth, hit);
                }
            }

            if (!hit.ChargingAttack)
                _comboState++;

            _isAttacking = false;
            _timeLastHit = Time.time;
        }

        private void MakeDamage(Health target, Hit hit)
        {
            int hitDamage = hit.Damage;
            if (hit.ChargingAttack)
            {
                var timeCharged = (_timeCharged) / MaxTimeCharging;
                var f = hit.DamageProgression.Evaluate(timeCharged);
                Debug.Log(timeCharged + "->" + f);
                hitDamage = Mathf.RoundToInt(hit.MinDamage +
                                             ((hit.Damage - hit.MinDamage) *
                                              f));
            }

            target.TakeDamage(hitDamage);

//VFX
            var vfx = new GameObject("Attack VFX");
            vfx.transform.position = target.transform.position;
            var vfxve = vfx.AddComponent<VisualEffect>();
            vfxve.visualEffectAsset = AttackVisualEffect;
            vfxve.SetUInt("NbParticule", hit.NbParticule);
            vfxve.SetFloat("Duration", hit.Duration);
            vfxve.SetFloat("MinLifeDuration", hit.LifeDuration.x);
            vfxve.SetFloat("MaxLifeDuration", hit.LifeDuration.y);
            vfxve.SetFloat("Velocity", hit.Velocity);
            if (!(hit.ColorOverLife is null))
                vfxve.SetGradient("ColorOverLife", hit.ColorOverLife);
            if (!(hit.ParticuleTexture is null))
                vfxve.SetTexture("Texture", hit.ParticuleTexture);
            vfxve.SendEvent("OnPlay");
            vfxve.Play();
            Destroy(vfx, 0.5f);
        }
    }
}