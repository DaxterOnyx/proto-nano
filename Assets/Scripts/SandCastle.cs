﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(Health))]
public class SandCastle : MonoBehaviour
{
    public Health Health;
    public VisualEffectAsset DestroyEffect;

    void Start()
    {
        Health.onDeath += (object obj, System.EventArgs ev) =>
        {
            var vfx = new GameObject("Destroy VFX");
            vfx.transform.position = transform.position;
            var vfxve = vfx.AddComponent<VisualEffect>();
            vfxve.visualEffectAsset = DestroyEffect;
            vfxve.Play();

            Destroy(vfx, 1.0f);
        };
    }
}
