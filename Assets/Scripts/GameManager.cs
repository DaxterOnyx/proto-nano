﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void OnQuit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    // Update is called once per frame
    void OnReload()
    {
        Debug.Log("Reload");
        SceneManager.LoadScene(0);
    }
}
