﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(VisualEffect))]
public class WaterShot : MonoBehaviour
{
    [HideInInspector] public GameObject shooter; // The player who shot the water

    public int Damages;
    public LayerMask TargetsLayer;
    
    private bool _waitForDestroy;
    private Rigidbody _rigidbody;
    private VisualEffect _effect;

	private void Start()
	{
        _waitForDestroy = false;
        _rigidbody = GetComponent<Rigidbody>();
        _effect = GetComponent<VisualEffect>();

    }

	private void OnTriggerEnter(Collider other)
	{
        if (_waitForDestroy) return;
        bool isTarget = (TargetsLayer.value | (1 << other.gameObject.layer)) > 0;
        if (isTarget && other.gameObject != shooter)
		{
            Health targetHealth = other.GetComponent<Health>();
            targetHealth?.TakeDamage(Damages);
        }

        _waitForDestroy = true;
        _rigidbody.isKinematic = true;
        _effect.Stop();
        Destroy(gameObject, 3.0f); // wait for particles to disappear
    }
}
