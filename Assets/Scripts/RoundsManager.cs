﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundsManager : MonoBehaviour
{
    public static RoundsManager instance = null;

    private int[] _scores;

    public int[] Scores {
        get { return _scores; }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            _scores = new int[2] { 0, 0 };
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

	public void WinRoundFor(int index)
	{
        _scores[index]++;
        SceneManager.LoadScene(0);
    }
}
