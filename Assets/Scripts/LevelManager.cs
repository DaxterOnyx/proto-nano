﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Header("Size")]
    [OnValueChanged("ChangeSize")]
    public float Width = 10;
    [OnValueChanged("ChangeSize")]
    public float Depth = 5;
    [OnValueChanged("ChangeSize")]
    public float Height = 5;
    
    [Header("GameObjects")]
    public Transform Ground;
    public Transform NorthWall;
    public Transform EstWall;
    public Transform WestWall;
    public Transform SouthWall;
    public Transform Roof;

    private Collider _collider;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider>();

        ChangeSize();
    }

    private void ChangeSize()
    {
        NorthWall.localScale =      new Vector3(Width,1,1);
        NorthWall.localPosition =   new Vector3(0    ,0,Depth/2);
        
        SouthWall.localScale =      new Vector3(Width,1,1);
        SouthWall.localPosition =   new Vector3(0    ,0,-Depth/2);
        
        EstWall.localScale =        new Vector3(1       ,1,Depth);
        EstWall.localPosition =     new Vector3(Width/2 ,0,0);
        
        WestWall.localScale =       new Vector3(1       ,1,Depth);
        WestWall.localPosition =    new Vector3(-Width/2,0, 0);

        Ground.localScale =         new Vector3(Width, 1    , Depth);
        Roof.localScale =           new Vector3(Width,1     ,Depth);
        Roof.localPosition =        new Vector3(0    ,Height,0);
    }
}
