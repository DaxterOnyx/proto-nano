﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoresLabel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] scores = RoundsManager.instance.Scores;
        TextMeshProUGUI mesh = GetComponent<TextMeshProUGUI>();
        mesh.text = scores[0] + " - " + scores[1];
    }
}
