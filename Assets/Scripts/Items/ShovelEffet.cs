﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShovelEffet : ItemEffect
{
	public float BuildRange = 1.0f;
	public GameObject CastlePrefab;

	public bool buildOnGrid = true;

	private Indicators _preview;
	private GridSystem _grid;
	private Vector3 _buildLocation;
	private Quaternion _buildRotation;
	private bool _buildValid;

	protected void Awake()
	{
		base.Awake();
		_grid = FindObjectOfType<GridSystem>();
		_item.onPickUp += OnPickUp;
		_item.onDrop += OnUnparent;
		_item.onBreak += OnUnparent;
	}

	protected void Update()
	{
		base.Update();

		if (!_item.Owner) return;

		_buildLocation = GetBuildLocation(_item.Owner.transform);
		_buildRotation = buildOnGrid ? Quaternion.identity : _item.Owner.transform.rotation;
		_buildValid = _grid.CellFreeAt(_buildLocation);

		if (!_preview) return;

		_preview.MoveBuildPreview(_buildLocation, _buildRotation, _buildValid);
	}

	public override bool DoEffect()
	{
		if (!_buildValid)
		{
			return false;
		}

		GameObject castle = Instantiate(CastlePrefab, _buildLocation, _buildRotation);
		_grid.AddObject(castle);
		return true;
	}

	private Vector3 GetBuildLocation(Transform from)
	{
		Vector3 offset = from.forward * ((from.lossyScale.z + CastlePrefab.transform.lossyScale.z) / 2 + BuildRange);
		// Vertically align to the ground
		offset += Vector3.up * (-from.position.y + CastlePrefab.transform.lossyScale.y / 2);

		Vector3 basePosition = from.position + offset;

		return !buildOnGrid ? basePosition : _grid.GetSnapedPosition(basePosition);
	}

	private void OnDrawGizmosSelected()
	{
		return;
		Hand[] players = FindObjectsOfType<Hand>();
		Mesh mesh = CastlePrefab.GetComponent<MeshFilter>().sharedMesh;

		foreach (var player in players)
		{
			Vector3 buildLocation = GetBuildLocation(player.transform);
			if (_grid.CellFreeAt(buildLocation))
			{
				Gizmos.color = Color.green;
			}
			else
			{
				Gizmos.color = Color.red;
			}
			Quaternion rotation = buildOnGrid ? Quaternion.identity : _item.Owner.transform.rotation;
			Gizmos.DrawWireMesh(mesh, buildLocation, rotation, CastlePrefab.transform.localScale);
		}
	}

	private void OnPickUp(object sender, EventArgs args)
	{
		_preview = _item.Owner.GetComponent<Indicators>();
		_preview.EnableBuildPreview(true);
	}

	private void OnUnparent(object sender, EventArgs args)
	{
		_preview.EnableBuildPreview(false);
		_preview = null;
	}
}