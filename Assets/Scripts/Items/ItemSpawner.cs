﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
   [System.Serializable]
   struct WeightedItem
	{
        [HorizontalGroup("WeightedItem")]
        public ItemEffect prefab;

        [HideLabel]
        [HorizontalGroup("WeightedItem", 40)]
        public float weight;
	}

    [HideInInspector]
    [AssetSelector,InlineEditor(InlineEditorObjectFieldModes.Foldout)]
    public ItemEffect[] ItemPrefabs;

    [SerializeField]
    private WeightedItem[] WeightedItemPrefabs;
    private float _totalWeight = 0;

    public int MaxItems = 1;
    [MinMaxSlider(0.2f, 15.0f)] public Vector2 SpawnTimeInterval;
    public Bounds SpawnArea;

    private List<GameObject> _currentItems;
    private float _timeBeforeNextItem;

    void Start()
    {
        _currentItems = new List<GameObject>();
        _timeBeforeNextItem += GetRandomSpawnDelay();

        // Compute total item weight
        foreach (WeightedItem item in WeightedItemPrefabs)
            _totalWeight += item.weight;
    }

    private void SpawnItem()
    {
        if (_currentItems.Count >= MaxItems)
            return;

        GameObject prefab = GetRandomItemInWeightedPool();

        var item = Instantiate(prefab, GetRandomPosition(), Quaternion.identity);
        _currentItems.Add(item);

        item.GetComponent<HandItem>().onBreak += (sender, e) =>_currentItems.Remove(item);
    }

    protected void Update()
    {
        _timeBeforeNextItem -= Time.deltaTime;

        if (_timeBeforeNextItem <= 0.0f)
        {
            SpawnItem();
            _timeBeforeNextItem = GetRandomSpawnDelay();
        }
    }

    private Vector3 GetRandomPosition()
    {
        float x = Random.Range(SpawnArea.min.x, SpawnArea.max.x);
        float z = Random.Range(SpawnArea.min.z, SpawnArea.max.z);
        return new Vector3(x, 0.0f, z);
    }

    private float GetRandomSpawnDelay()
	{
        return Random.Range(SpawnTimeInterval.x, SpawnTimeInterval.y);
	}

    [System.Obsolete("You shouldn't use a non weighted pick method anymore")]
    private GameObject GetRandomItemInPool()
    {
        int index = Random.Range(0, ItemPrefabs.Length);
        return ItemPrefabs[index].gameObject;
    }

    private GameObject GetRandomItemInWeightedPool()
    {
        float weight = Random.Range(0.0f, _totalWeight);
        float currentWeight = 0.0f;

        foreach (WeightedItem item in WeightedItemPrefabs) {
            currentWeight += item.weight;
            if (weight <= currentWeight)
                return item.prefab.gameObject;
        }

        return null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(SpawnArea.center, SpawnArea.extents * 2);
    }
}
