﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HandItem))]
public abstract class ItemEffect : MonoBehaviour
{
    public event EventHandler onNoUsesLeft;

    public float UseCooldown = 0.5f;
    [Tooltip("-1 for unlimited uses")] public int MaxUses = 1;

    protected HandItem _item;
    
    private float _timeBeforeUse;
    private int _usesLeft;
    
    protected void Awake()
    {
        _item = GetComponent<HandItem>();
        _item.onPickUp += OnPickUp;

        _usesLeft = MaxUses;
    }

    public void Perform()
	{
        if (_timeBeforeUse > 0.0f) return;

        bool success = DoEffect();
        _timeBeforeUse = UseCooldown;
        if (success) _usesLeft--;

        if (_usesLeft == 0)
		{
            onNoUsesLeft?.Invoke(this, EventArgs.Empty);
		}
	}

	public abstract bool DoEffect();

    protected void Update()
    {
        _timeBeforeUse -= Time.deltaTime;
    }

    private void OnPickUp(object sender, EventArgs args)
    {
        _timeBeforeUse = UseCooldown;
    }
}
