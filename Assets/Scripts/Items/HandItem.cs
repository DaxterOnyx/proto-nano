﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HandItem : MonoBehaviour
{
    public event EventHandler onPickUp;
    public event EventHandler onDrop;
    public event EventHandler onBreak;

    public Hand Owner { get; private set; }

    private ItemEffect _effect;

    public GameObject OnGroundEffect;

	void Awake()
	{
        _effect = GetComponent<ItemEffect>();

        if (_effect)
            _effect.onNoUsesLeft += OnNoUsesLeft;
	}

	public void Pick(Hand owner)
	{
        if (Owner) return; // Can't steal item to another player !

        Owner = owner;
        if (_effect) _effect.enabled = true;

        transform.SetParent(owner.HandPosition);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        onPickUp?.Invoke(this, EventArgs.Empty);
        OnGroundEffect?.SetActive(false);
	}

    public void Drop(Transform destination)
	{
        Owner = null;
        if (_effect) _effect.enabled = false;

        transform.SetParent(null);
        transform.position = destination.position;
        transform.rotation = destination.rotation;

        onDrop?.Invoke(this, EventArgs.Empty);
        OnGroundEffect?.SetActive(true);
	}

    public void Use()
    {
        if (!Owner || !_effect) return;
        _effect.Perform();
    }

    private void OnNoUsesLeft(object sender, EventArgs e)
    {
        onBreak?.Invoke(this, EventArgs.Empty);
        Destroy(gameObject);
    }
}
