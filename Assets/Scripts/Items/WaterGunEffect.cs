﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGunEffect : ItemEffect
{
	public float ShotForce = 2.0f;
	public float ShotHeight = 0.2f;
	public GameObject WaterShotPrefab;

	public Transform CannonLocation;

	public override bool DoEffect()
	{
		GameObject shot = Instantiate(WaterShotPrefab, CannonLocation.transform.position, Quaternion.identity);
		Vector3 direction = _item.Owner.transform.forward + Vector3.up * ShotHeight;
		shot.GetComponent<Rigidbody>().AddForce(direction * ShotForce, ForceMode.Impulse);
		shot.GetComponent<WaterShot>().shooter = _item.Owner.gameObject;

		return true;
	}

	/*private void OnDrawGizmosSelected()
	{
		Hand[] players = FindObjectsOfType<Hand>();
		Mesh mesh = CastlePrefab.GetComponent<MeshFilter>().sharedMesh;

		foreach (var player in players)
		{
			Vector3 buildLocation = GetBuildLocation(player.transform);
			if (_grid.CellFreeAt(buildLocation))
			{
				Gizmos.color = Color.green;
			}
			else
			{
				Gizmos.color = Color.red;
			}
			Quaternion rotation = buildOnGrid ? Quaternion.identity : _item.Owner.transform.rotation;
			Gizmos.DrawWireMesh(mesh, buildLocation, rotation, CastlePrefab.transform.localScale);
		}
	}*/
}