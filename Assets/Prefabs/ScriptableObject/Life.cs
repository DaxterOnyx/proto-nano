﻿using UnityEngine;

[CreateAssetMenu(fileName = "Life", menuName = "ScriptableObjects/Life", order = 0)]
public class Life : ScriptableObject
{
    public uint Health;
}