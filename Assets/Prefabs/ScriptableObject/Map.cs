﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Map", menuName = "ScriptableObjects/Map", order = 3)]
public class Map : ScriptableObject
{
    public List<GridSystem.Cell> IndestructibleCastle = new List<GridSystem.Cell>();
    public List<GridSystem.Cell> DestructibleCastle = new   List<GridSystem.Cell>();
}
