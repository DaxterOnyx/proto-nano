﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "Hit", menuName = "ScriptableObjects/Hit", order = 2)]
public class Hit : ScriptableObject
{
    public enum Signal
    {
        None = 0,
        Melee,
        Distance,
    }
    [Header("Input")]
    [Tooltip("Input to Enter")]
    public Signal Action = Signal.None;
    [Tooltip("This attack change behaviour depending on time pressed")]
    public bool ChargingAttack = false;
    [Tooltip("Minimal time to Keep Input pressed for the Hit is considered, animation begin after"),Min(0)]
    public float MinTimePress = 0;
    [Tooltip("Max time to Keep Input pressed, after this time launch attack for charging attack, not launch attack in not Charging Attack"),Min(0)]
    public float MaxTimePress = 0;
    [Header("Animation")]
    [Tooltip("Time between end of the input/start of animation and the hit"),Min(0)]
    public float AnticipationTime;
    [Tooltip("Time between hit and listen other input (in theory), the time between the hit and the end of animation"),Min(0)]
    public float RecoveryTime;
    [Header("Hit")]
    [Min(0)]
    public int Damage = 10;
    [Tooltip("Damage deal if charged attack is not charged"),Min(0),ShowIf("ChargingAttack")]
    public int MinDamage = 10;
    [Tooltip("Damage deal depending on time attack has been charged"),ShowIf("ChargingAttack")]
    public AnimationCurve DamageProgression = AnimationCurve.Linear(0,0,1,1);
    public Vector3 Range = Vector3.forward;
    public float AreaOfAttack = 0.5f;

    [Header("VFXEffect")] 
    [Min(0)] 
    public uint NbParticule = 32;
    [Min(0)]
    public float Duration = 0.1f;
    public Vector2 LifeDuration = new Vector2(0.1f,0.5f);
    [Min(0)] 
    public float Velocity = 10;
    public Gradient ColorOverLife;
    public Texture ParticuleTexture;
}
