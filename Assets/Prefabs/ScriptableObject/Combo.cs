﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Combo", menuName = "ScriptableObjects/Combo", order = 1)]
public class Combo : ScriptableObject
{
    [AssetSelector,InlineEditor(InlineEditorObjectFieldModes.Foldout)]
    public List<Hit> Hits = new List<Hit>();
}
