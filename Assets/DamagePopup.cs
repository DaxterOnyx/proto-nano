﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamagePopup : MonoBehaviour
{
    public void Setup(int value, Color popupColor)
    {
        if (!(Camera.main is null)) transform.rotation = Camera.main.transform.rotation;
        var componentInChildren = GetComponentInChildren<TextMeshPro>();
        componentInChildren.text = value.ToString();
        componentInChildren.color = popupColor;
    }

    public void EndAnim()
    {
        Destroy(gameObject);
    }
}
