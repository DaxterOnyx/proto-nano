﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;


namespace ControllerVisualizer
{
    [SelectionBase]
    public class InputVisualizer : MonoBehaviour
    {
        [Tooltip("Specifies which color the material should become when used.")]
        [SerializeField] private Color activatedColor = Color.white;

        [Tooltip("If multiple controls match 'Control Path' at runtime, this property decides "
            + "which control to visualize from the list of candidates. It is a zero-based index.")]
        public int deviceId;

        [SerializeField] private InputAction press = null;
        [SerializeField][Range(1, 2)] private float bigger = 1;

        [SerializeField] private InputAction stick = null;
        [SerializeField][Range(0, 3)] private float range = 0;

        [SerializeField] private InputAction trigger = null;
        private Slider slider;

        private Vector2 parentPos;

        private RectTransform RTransform => GetComponent<RectTransform>();
        
        void OnEnable()
        {
            press?.Enable();
            stick?.Enable();
            if (trigger != null) {
                trigger.Enable();
                slider = GetComponent<Slider>();
            }
        }

        private void OnDisable() {
            press?.Disable();
            stick?.Disable();
        }

        private void Awake()
        {
            if (press != null)
            {
                press.performed += ctx => OnPressPerformed(ctx);
                press.canceled += ctx => OnPressCanceled(ctx);
            }
            if (stick != null)
            {
                stick.performed += ctx => OnStickPerformed(ctx.ReadValue<Vector2>());
                stick.canceled += ctx => OnStickPerformed(ctx.ReadValue<Vector2>());
            }
            if (trigger != null)
            {
                trigger.performed += ctx => OnTriggerPerformed(ctx.ReadValue<float>());
                trigger.canceled += ctx => OnTriggerPerformed(ctx.ReadValue<float>());
            }
        }

        private void OnPressPerformed(InputAction.CallbackContext ctx)
        {
            if (press.activeControl.device.deviceId != deviceId) return;
            SwapColor();
            RTransform.localScale *= bigger;
        }

        private void OnPressCanceled(InputAction.CallbackContext ctx)
        {
            if (press.activeControl.device.deviceId != deviceId) return;
            SwapColor();
            RTransform.localScale /= bigger;   
        }

        private void OnTriggerPerformed(float value)
        {
            if (trigger.activeControl.device.deviceId != deviceId) return;
            this.slider.value = value;
        }

        private void SwapColor()
        {
            Image image = GetComponent<Image>();
            Color color = activatedColor;
            activatedColor = image.color;
            image.color = color;
        }
    
        private void OnStickPerformed(Vector2 value)
        {
            if (stick.activeControl.device.deviceId != deviceId) return;
            Vector3 position = Vector3.zero;
            RTransform.localPosition = new Vector3(position.x + (value.x * range), position.y +(value.y * range), position.z);
        }

        private void OnDrawGizmosSelected() {
            if(range == 0) return;
            Gizmos.DrawWireSphere(RTransform.position, range * transform.parent.parent.localScale.x);
        }
    }
}
